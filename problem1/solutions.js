const fs = require('fs');

/*Q1. Read the result.json and gather data of all the people who failed in both their exams.
Less than 35 score is failed.*/

function getFailedPeople() {
    fs.readFile("./result.json", "utf-8", (err, obj) => {
        if (err) {
            console.error(err)
        } else {
            if (JSON.parse(obj)) {
                const tempObj = JSON.parse(obj)
                const result = Object.keys(tempObj).reduce((acc, curr) => {
                    if (tempObj[curr][0].Mathematics < 35 && tempObj[curr][0]["Computer Science"] < 35) {
                        acc[curr] = [tempObj[curr][0]];
                    }
                    return acc;
                }, {})
                console.log(result);
            }
        }
    })
}
//console.log(getFailedPeople())

//Q2. Gather data of students who has failed in only computer science.

function onlyOneSubject() {
    fs.readFile("./result.json", "utf-8", (err, obj) => {
        if (err) {
            console.error(err)
        } else {
            if (JSON.parse(obj)) {
                const tempObj = JSON.parse(obj)
                const result = Object.keys(tempObj).reduce((acc, curr) => {
                    if (tempObj[curr][0]["Computer Science"] < 35) {
                        // return acc.push(curr)
                        acc[curr] = tempObj[curr][0];
                    }
                    return acc;
                }, {})
                console.log(result);
            }
        }
    })
}

//onlyOneSubject()

// Q3. Calculate average for each student and save it to a new file.

function getAverage() {
    fs.readFile("./result.json", "utf-8", (err, obj) => {
        if (err) {
            console.error(err)
        } else {
            if (JSON.parse(obj)) {
                const tempObj = JSON.parse(obj);
                const result = Object.keys(tempObj).reduce((acc, curr) => {
                    acc[curr] = (tempObj[curr][0]["Mathematics"] + tempObj[curr][0]["Computer Science"]) / 2
                    return acc;
                }, {})
                fs.writeFileSync("average.json", JSON.stringify(result), (err) => {
                    console.error(err);
                })
                console.log(result);
            }
        }
    })
}
// getAverage()


//Q4. Sort data with student name and sort the subjects array on alphabetical order as well.

function getSortedName(obj) {
    fs.readFile("./result.json", "utf-8", (err, obj) => {
        if (err) {
            console.error(err)
        } else {
            if (JSON.parse(obj)) {
                const tempObj = JSON.parse(obj);
                const name = Object.keys(tempObj).sort();
                const result = name.reduce((acc, curr) => {
                    acc[curr] = [{
                        [Object.keys(tempObj[curr][0]).sort()[0]]: tempObj[curr][0][Object.keys(tempObj[curr][0]).sort()[0]],
                        [Object.keys(tempObj[curr][0]).sort()[1]]: tempObj[curr][0][Object.keys(tempObj[curr][0]).sort()[1]]
                    }]
                    return acc
                }, {})
                console.log(result);
            }
        }
    })
}
//getSortedName();

/*Q5. Generate result of user in this form and write it to a new file.
ComputerScience - computersciencemarks , Mathematics - mathematicsMarks : studentName

 Display result in each separate line.*/

function displayData() {
    fs.readFile("./result.json", "utf-8", (err, obj) => {
        if (err) {
            console.error(err)
        } else {
            if(JSON.parse(obj)){
                const tempObj=JSON.parse(obj)
                const result = Object.keys(tempObj).reduce((acc, curr) => {
                    acc["Computer Science-" + tempObj[curr][0]["Computer Science"] + "," + "Mathematics" + "-" + tempObj[curr][0]["Mathematics"]] = curr
                    //console.log(acc)
                    return acc;
                },{})
                fs.writeFileSync("Student_result.json", JSON.stringify(result), (err) => {
                    console.error(err);
                })
                return result;

            }
        }
    })
}

//displayData()