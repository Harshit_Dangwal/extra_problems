const fs = require("fs");
/*Q.1 Read data from data.json file
    Write a function to get countries from a pair of lat long. 
    Get countries for each user and add additional field "country" to data.json inside ..address field.
*/

function getCountries() {
    fs.readFile("data.json", "utf-8", (err, data1) => {
        if (err) {
            console.error(err);
        } else {
            fs.readFile("country.json", "utf-8", (err, data2) => {
                if (err) {
                    console.error(err);
                } else {
                    if (JSON.parse(data1) && JSON.parse(data2)) {
                        dataObj1 = JSON.parse(data1);
                        dataObj2 = JSON.parse(data2);
                        const result = dataObj1.reduce((acc, curr) => {
                            if (Object.keys(dataObj2).find((value) => {
                                if (curr.address.coordinates.lat + " | " + curr.address.coordinates.lng === value | parseFloat(value) === parseFloat(curr.address.coordinates.lat + " | " + curr.address.coordinates.lng)) {
                                    curr.address["country"] = dataObj2[value];
                                    acc.push(curr);
                                }
                            })) {
                            }
                            return acc;
                        }, [])
                        if (result) {
                            fs.writeFile("data.json", JSON.stringify(result), (err) => {
                                if (err) {
                                    console.error(err)
                                }
                            })
                        }
                    }
                }
            })
        }
    })
}
//getCountries();

// Q2. Delete record from data json having age less than 18.

function deleteRecord() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error(err);
        } else {
            if (JSON.parse(data)) {
                const dataObj = JSON.parse(data);
                const result = dataObj.filter((curr) => {
                    if (curr.age < 18) {
                        delete curr.age;
                    } else {
                        return curr;
                    }
                })
                if (result) {
                    fs.writeFile("data.json", JSON.stringify(result), (err) => {
                        if (err) {
                            console.error(err);
                        }
                    })
                }
            }
        }
    })
}

console.log(deleteRecord())