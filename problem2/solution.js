const users = [{
    firstName: "Emily",
    lastName: "Blunt",
    age: 23
}, {
    firstName: "Carlos",
    lastName: "Vela",
    age: 23
}, {
    firstName: "Marcus",
    lastName: "Arnold",
    age: 21
}, {
    firstName: "Leo",
    lastName: "Wesley",
    age: 24
}]

// Q1. Whats the average age of the users.

function getAverageAge(users) {
    const result = users.reduce((acc, curr) => {
        if (curr.age) {
            acc += curr.age;
        }
        return acc;
    }, 0)
    return result / users.length;
}
/*-----------------------------------------------------------/*

// Q2. Transform users array to give just firstName and lastName for each user.

function getOnlyName(users) {
    const result = users.reduce((acc, curr) => {
        acc.push({ ['firstName']: curr.firstName, ['lastName']: curr.lastName })
        return acc;
    }, [])
    return result;
};
/*-----------------------------------------------------------/*

/*Q3. Get all fullNames of each user sorted in alphabetical order.
 LastName comes before FirstName. */

function getSortedNames(users) {
    sortedNames = users.sort((a, b) => {
        if (a.firstName < b.firstName) {
            return -1;
        } else {
            return 1;
        }
    })
    const result = sortedNames.map((curr) => {
        return curr.lastName + " " + curr.firstName;

    })
    return result;
}

/*-----------------------------------------------------------/*

/*Q4. Transform users array to 
    [
        ["Emily", "Blunt", 23],
        ["Vela", "Carlos", 23], 
        ["Marcus", "Arnold", 21], 
        ["Wesley", "Leo", 24 ]
    ]
*/

function getUsersArray(users) {
    let counter = 0;
    const result = users.map((curr) => {
        counter += 1;
        if (counter % 2 != 0) {
            return [curr.firstName, curr.lastName, curr.age]
        } else {
            return [curr.lastName, curr.firstName, curr.age]
        }
    })
    return result;
}
/*-----------------------------------------------------------/*

/*Q5. Transform users array to 
[
    "Emily | Blunt, age: 23",
    "Carlos | Vela, age: 23", 
    "Marcus | Arnold, age: 21", 
    "Wesley | Leo, age: 24"
]
*/

function displayUsersArray(users) {
    let counter = 0
    const result = users.map((curr) => {
        counter++;
        if (counter != users.length) {
            return curr.firstName+" | "+curr.lastName+", "+"age: "+curr.age;
        } else {
            return curr.lastName+" | "+curr.firstName+", "+"age: "+curr.age;
        }
    })
    return result;
}