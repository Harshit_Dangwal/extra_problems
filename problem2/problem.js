const users = [{
    firstName: "Emily",
    lastName: "Blunt",
    age: 23
}, {
    firstName: "Carlos",
    lastName: "Vela",
    age: 23
}, {
    firstName: "Marcus",
    lastName: "Arnold",
    age: 21
}, {

    firstName: "Leo",
    lastName: "Wesley",
    age: 24

}]


/*

    Q. Whats the average age of the users.
    Q. Transform users array to give just firstName and lastName for each user.
    Q. Get all fullNames of each user sorted in alphabetical order.
        LastName comes before FirstName.
    Q. Transform users array to 
    [
        ["Emily", "Blunt", 23],
        ["Vela", "Carlos", 23], 
        ["Marcus", "Arnold", 21], 
        ["Wesley", "Leo", 24 ]
    ]

    Q. Transform users array to 
        [
            "Emily | Blunt, age: 23",
            "Carlos | Vela, age: 23", 
            "Marcus | Arnold, age: 21", 
            "Wesley | Leo, age: 24"
        ]

*/